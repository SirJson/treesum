use log::*;
use metrohash::MetroHash;
use std::error;
use std::fmt;
use std::fs::{self, File, ReadDir};
use std::hash::Hasher;
use std::io::{self, Read, Write};
use std::str;

type Result<T> = std::result::Result<T, ChecksumError>;

#[derive(Debug)]
pub enum Success {
    CreatedFromScratch(u64),
    DirectoryUnchanged(u64, u64),
}

impl fmt::Display for Success {
    fn fmt(&self, formater: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            Success::CreatedFromScratch(sum) => write!(
                formater,
                "Created checksum file from scratch! The checksum is: {:#X}",
                sum
            ),
            Success::DirectoryUnchanged(old, new) => write!(
                formater,
                "The target directory is not modified! Old({:#X}) == New({:#X})",
                old, new
            ),
        }
    }
}

#[derive(Debug)]
pub struct ChecksumError {
    pub kind: ErrorKind,
}

#[derive(Debug)]
pub enum ErrorKind {
    ChecksumMismatch,
    TargetNotDirectory,
    InvalidData,
    IOError(io::Error),
}

impl From<io::Error> for ChecksumError {
    fn from(error: io::Error) -> Self {
        ChecksumError {
            kind: ErrorKind::IOError(error),
        }
    }
}

impl From<ErrorKind> for ChecksumError {
    fn from(error: ErrorKind) -> Self {
        ChecksumError { kind: error }
    }
}

impl fmt::Display for ChecksumError {
    fn fmt(&self, formater: &mut fmt::Formatter) -> fmt::Result {
        match &self.kind {
            ErrorKind::ChecksumMismatch => write!(
                formater,
                "Checksum mismatch detected! Your target directory is modified."
            ),
            ErrorKind::TargetNotDirectory => write!(formater, "The target is not a directory"),
            ErrorKind::InvalidData => write!(
                formater,
                "The checksum file contains invalid data that can not be parsed"
            ),
            ErrorKind::IOError(io_error) => {
                write!(formater, "Checksum compute failed! IO Error: {}", io_error)
            }
        }
    }
}

impl error::Error for ChecksumError {
    fn description(&self) -> &str {
        "Failed to compute checksum for directory"
    }

    fn cause(&self) -> Option<&error::Error> {
        match &self.kind {
            ErrorKind::IOError(err) => Some(err),
            _ => None,
        }
    }
}

fn add_to_checksum(mut file: File, mut buffer: &mut Vec<u8>, hash: &mut MetroHash) -> Result<()> {
    let file_len = file.read_to_end(&mut buffer)?;
    hash.write(&mut buffer[0..file_len]);
    buffer.clear();
    Ok(())
}

fn walk_dir(dir: ReadDir, mut buffer: &mut Vec<u8>, mut hash: &mut MetroHash) -> Result<()> {
    for entry in dir {
        if let Ok(entry) = entry {
            let info = entry.metadata()?;
            if info.is_dir() {
                debug!("\tOpen directory {:?}...", entry.path());
                walk_dir(entry.path().read_dir()?, &mut buffer, &mut hash)?;
            } else if info.is_file() {
                debug!("\t\tHashing {:?}...", entry.path());
                add_to_checksum(File::open(entry.path())?, &mut buffer, &mut hash)?;
            }
        }
    }

    Ok(())
}

/// Calculates a single checksum of "t'arget_dir"
pub fn compute(target_dir: &str) -> Result<u64> {
    debug!("Init MetroHash...");
    let mut root_hash: MetroHash = MetroHash::new();

    debug!("Pre-allocate buffer...");
    let mut buffer: Vec<u8> = Vec::with_capacity(1024);

    debug!("Resolving target directory: {}", target_dir);
    let target_path = fs::canonicalize(target_dir)?;

    if !target_path.is_dir() {
        return Err(ChecksumError::from(ErrorKind::TargetNotDirectory));
    }

    debug!("Start directory = {}", target_dir);

    walk_dir(target_path.read_dir()?, &mut buffer, &mut root_hash)?;

    Ok(root_hash.finish())
}

fn write_checksum_file(checksum_file: &str, result: u64) -> Result<Success> {
    let mut handle = File::create(checksum_file)?;

    debug!("Writing checksum data...");
    let data = format!("{}", result).into_bytes();
    handle.write(&data)?;

    if let Err(error) = handle.sync_all() {
        warn!("Checksum file sync to disk failed! You might have to wait a little bit before you unmout your device if you plan to. OS Error: {:?}", error);
    }
    Ok(Success::CreatedFromScratch(result))
}

pub fn create_checksum_file(target_dir: &str, checksum_file: &str) -> Result<Success> {
    debug!("Createing new checksum file...");
    let result = compute(target_dir)?;
    write_checksum_file(checksum_file, result)
}

/// Checks if target_dir changed with the help of a checksum file.
/// If the path you provied to that checksum_file does not exist yet it will be created for you
pub fn check(target_dir: &str, checksum_file: &str) -> Result<Success> {
    debug!("Computing checksum of directory tree...");
    let result = compute(target_dir)?;

    debug!("Check for differences or create new checksum file...");
    match fs::canonicalize(checksum_file) {
        Ok(path) => {
            debug!("Found already calculated checksum file!");
            let mut handle = File::open(path)?;
            debug!("Loading old checksum...");
            let mut data: Vec<u8> = Vec::new();
            handle.read_to_end(&mut data)?;
            match str::from_utf8(&data) {
                Ok(content) => {
                    let saved_hash = match content.parse::<u64>() {
                        Ok(val) => val,
                        Err(_) => return Err(ChecksumError::from(ErrorKind::InvalidData)),
                    };

                    if result != saved_hash {
                        let output = ChecksumError::from(ErrorKind::ChecksumMismatch);
                        return Err(output);
                    } else {
                        return Ok(Success::DirectoryUnchanged(result, saved_hash));
                    }
                }
                Err(_) => Err(ChecksumError::from(ErrorKind::InvalidData)),
            }
        }
        Err(_) => {
            debug!(
                "Failed to canonicalize path: \"{}\". Trying creating the file now...",
                checksum_file
            );

            write_checksum_file(checksum_file, result)
        }
    }
}
