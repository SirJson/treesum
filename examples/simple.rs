use fern;
use log::*;
use rand::distributions::Alphanumeric;
use rand::{self, Rng};
use std::fs;
use std::thread;
use std::time::Duration;
use treesum;

const EXAMPLE_TREE: &'static str = "./examples/files/ytdl";
const EXAMPLE_CHK_FILE: &'static str = "./examples/ytdl.chk";

fn recheck_tree() {
    match treesum::check(EXAMPLE_TREE, EXAMPLE_CHK_FILE) {
        Ok(result) => {
            info!("{}", result);
        }
        Err(error) => {
            error!("{}", error);
            let result = treesum::create_checksum_file(EXAMPLE_TREE, EXAMPLE_CHK_FILE)
                .expect("Failed to create new checksum file");
            info!("Done: {}", result);
        }
    }
}

fn main() {
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "[{}]::<{}> => {}",
                record.level(),
                record.target(),
                message,
            ))
        }).level(log::LevelFilter::Debug)
        .chain(std::io::stdout())
        .apply()
        .expect("Log init failed!");

    info!("This example is testing treesum on youtube-dl.");
    info!("The normal use case would be in a build script. Waiting 6 seconds...\n");
    thread::sleep(Duration::from_secs(6));

    match treesum::check(EXAMPLE_TREE, EXAMPLE_CHK_FILE) {
        Ok(result) => {
            info!("{}", result);
            info!("\nAll okay! Let's rest 10 seconds and then modify something and see if everything is still okay\n");
            thread::sleep(Duration::from_secs(10));

            let data_count = rand::random::<u16>();
            info!("Writing {} chars of random data", data_count);
            let random_data = rand::thread_rng()
                .sample_iter(&Alphanumeric)
                .take(data_count as usize)
                .collect::<String>();

            fs::write(
                format!("{}/{}", EXAMPLE_TREE, "oh.no"),
                random_data.into_bytes(),
            ).expect("Where is the guru?");

            recheck_tree();
        }
        Err(error) => {
            error!("{}", error);
            treesum::create_checksum_file(EXAMPLE_TREE, EXAMPLE_CHK_FILE)
                .expect("Failed to create new checksum file");
        }
    }
}
